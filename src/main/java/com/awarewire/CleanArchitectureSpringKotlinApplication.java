package com.awarewire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanArchitectureSpringKotlinApplication {

	public static void main(String[] args) {
		SpringApplication.run(CleanArchitectureSpringKotlinApplication.class, args);
	}

}
